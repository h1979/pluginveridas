import { registerPlugin } from '@capacitor/core';

import type { VeridasFacePluginPlugin } from './definitions';

const VeridasFacePlugin = registerPlugin<VeridasFacePluginPlugin>(
  'VeridasFacePlugin',
  {
    web: () => import('./web').then(m => new m.VeridasFacePluginWeb()),
  },
);

export * from './definitions';
export { VeridasFacePlugin };
