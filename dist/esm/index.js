import { registerPlugin } from '@capacitor/core';
const VeridasFacePlugin = registerPlugin('VeridasFacePlugin', {
    web: () => import('./web').then(m => new m.VeridasFacePluginWeb()),
});
export * from './definitions';
export { VeridasFacePlugin };
//# sourceMappingURL=index.js.map