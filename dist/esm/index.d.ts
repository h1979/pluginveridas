import type { VeridasFacePluginPlugin } from './definitions';
declare const VeridasFacePlugin: VeridasFacePluginPlugin;
export * from './definitions';
export { VeridasFacePlugin };
