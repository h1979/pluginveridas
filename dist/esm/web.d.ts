import { WebPlugin } from '@capacitor/core';
import type { VeridasFacePluginPlugin } from './definitions';
export declare class VeridasFacePluginWeb extends WebPlugin implements VeridasFacePluginPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    getContacts(filter: string): Promise<{
        results: any[];
    }>;
}
