import { WebPlugin } from '@capacitor/core';
export class VeridasFacePluginWeb extends WebPlugin {
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
    async getContacts(filter) {
        console.log('filter: ', filter);
        return {
            results: [{
                    firstName: 'Dummy',
                    lastName: 'Entry',
                    telephone: '123456'
                }]
        };
    }
}
//# sourceMappingURL=web.js.map