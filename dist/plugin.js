var capacitorVeridasFacePlugin = (function (exports, core) {
    'use strict';

    const VeridasFacePlugin = core.registerPlugin('VeridasFacePlugin', {
        web: () => Promise.resolve().then(function () { return web; }).then(m => new m.VeridasFacePluginWeb()),
    });

    class VeridasFacePluginWeb extends core.WebPlugin {
        async echo(options) {
            console.log('ECHO', options);
            return options;
        }
        async getContacts(filter) {
            console.log('filter: ', filter);
            return {
                results: [{
                        firstName: 'Dummy',
                        lastName: 'Entry',
                        telephone: '123456'
                    }]
            };
        }
    }

    var web = /*#__PURE__*/Object.freeze({
        __proto__: null,
        VeridasFacePluginWeb: VeridasFacePluginWeb
    });

    exports.VeridasFacePlugin = VeridasFacePlugin;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

})({}, capacitorExports);
//# sourceMappingURL=plugin.js.map
