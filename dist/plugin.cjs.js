'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var core = require('@capacitor/core');

const VeridasFacePlugin = core.registerPlugin('VeridasFacePlugin', {
    web: () => Promise.resolve().then(function () { return web; }).then(m => new m.VeridasFacePluginWeb()),
});

class VeridasFacePluginWeb extends core.WebPlugin {
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
    async getContacts(filter) {
        console.log('filter: ', filter);
        return {
            results: [{
                    firstName: 'Dummy',
                    lastName: 'Entry',
                    telephone: '123456'
                }]
        };
    }
}

var web = /*#__PURE__*/Object.freeze({
    __proto__: null,
    VeridasFacePluginWeb: VeridasFacePluginWeb
});

exports.VeridasFacePlugin = VeridasFacePlugin;
//# sourceMappingURL=plugin.cjs.js.map
