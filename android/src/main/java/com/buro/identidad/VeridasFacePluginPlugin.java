package com.buro.identidad;

import android.util.Log;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "VeridasFacePlugin")
public class VeridasFacePluginPlugin extends Plugin {

    private VeridasFacePlugin implementation = new VeridasFacePlugin();

    @PluginMethod
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", implementation.echo(value));
        call.resolve(ret);
    }

    @PluginMethod
    public void getContacts(PluginCall call) {
        String value = call.getString("value");

        Log.i("getContacts", "!!!!");
        JSObject ret = new JSObject();
        ret.put("value", "asdasd");
        call.resolve(ret);
    }

    
}
