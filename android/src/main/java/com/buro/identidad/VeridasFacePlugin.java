package com.buro.identidad;

import android.util.Log;

public class VeridasFacePlugin {

    public String echo(String value) {
        Log.i("Echo", value);
        return value;
    }

    public String getContacts(String value) {
        Log.i("getContacts", value);
        return value;
    }
}
